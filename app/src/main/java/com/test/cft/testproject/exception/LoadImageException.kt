package com.test.cft.testproject.exception

/**
 * @author Denis Kopylov
 */
class LoadImageException(message: String) : Exception(message)