package com.test.cft.testproject.utils

import android.content.Context
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ResourceProvider @Inject constructor(private val context: Context) {

    fun getString(@StringRes id: Int): String = context.getString(id)
}