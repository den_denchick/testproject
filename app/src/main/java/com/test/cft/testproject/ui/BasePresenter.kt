package com.test.cft.testproject.ui

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * @author Denis Kopylov
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V>() {
    private val disposables = CompositeDisposable()

    private var onFinished = false

    protected fun Disposable.disposeOnFinish(): Disposable {
        disposables.add(this)
        return this
    }
}
