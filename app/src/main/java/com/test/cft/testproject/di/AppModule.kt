package com.test.cft.testproject.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * @author Denis Kopylov
 */
@Module
abstract class AppModule {

    @Binds
    abstract fun bindApplicationContext(application: Application): Context

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun provideContentResolver(context: Context) = context.contentResolver
    }
}