package com.test.cft.testproject.model

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class IdCreator @Inject constructor(private val context: Context) {

    fun getId(): Int {
        val pref: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val lastId = pref.getInt(APP_PREFERENCES_KEY_ID, -1)
        val newId = lastId + 1
        val editor = pref.edit()
        editor.putInt(APP_PREFERENCES_KEY_ID, newId)
        editor.apply()
        return newId
    }

    companion object {
        private const val APP_PREFERENCES = "pref"
        private const val APP_PREFERENCES_KEY_ID = "id"
    }
}