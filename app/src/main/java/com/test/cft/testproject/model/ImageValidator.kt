package com.test.cft.testproject.model

import com.test.cft.testproject.R
import com.test.cft.testproject.exception.BitmapConvertException
import com.test.cft.testproject.utils.ResourceProvider
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ImageValidator @Inject constructor(private val resourceProvider: ResourceProvider) {

    fun <T> validate(any: T?): T {
        any ?: throw BitmapConvertException(resourceProvider.getString(R.string.image_convertation_error))
        return any
    }
}