package com.test.cft.testproject.di

import com.test.cft.testproject.ui.LoadImageActivity
import dagger.Subcomponent

/**
 * @author Denis Kopylov
 */
@ActivityScope
@Subcomponent
interface ActivityComponent {

    fun inject(activity: LoadImageActivity)
}