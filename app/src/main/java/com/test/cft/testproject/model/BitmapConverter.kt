package com.test.cft.testproject.model

import android.content.ContentResolver
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import javax.inject.Inject
import android.graphics.Color
import android.graphics.Matrix
import com.test.cft.testproject.R
import com.test.cft.testproject.exception.BitmapConvertException
import com.test.cft.testproject.utils.ResourceProvider


/**
 * @author Denis Kopylov
 */
class BitmapConverter @Inject constructor(
    private val imageValidator: ImageValidator,
    private val resolver: ContentResolver,
    private val resourceProvider: ResourceProvider
) {

    fun convertUriToBitmap(uri: Uri?): Bitmap {
        val validUri = imageValidator.validate(uri)
        return imageValidator.validate(getBitmapFromUri(validUri))
    }

    fun inverseColorImage(bitmap: Bitmap?): Bitmap {
        val validBitmap = imageValidator.validate(bitmap)
        val width = validBitmap.width
        val height = validBitmap.height
        val bmOut = Bitmap.createBitmap(width, height, validBitmap.config)
        var A: Int
        var R: Int
        var G: Int
        var B: Int
        var pixel: Int
        for (x in 0 until width) {
            for (y in 0 until height) {
                pixel = validBitmap.getPixel(x, y)
                A = Color.alpha(pixel)
                R = Color.red(pixel)
                G = Color.green(pixel)
                B = Color.blue(pixel)
                var gray = (0.2989 * R + 0.5870 * G + 0.1140 * B).toInt()
                gray = if (gray > 128) {
                    255
                } else {
                    0
                }
                bmOut.setPixel(x, y, Color.argb(A, gray, gray, gray))
            }
        }
        return bmOut
    }

    fun mirrorBitmap(bitmap: Bitmap?): Bitmap {
        val validBitmap = imageValidator.validate(bitmap)
        val matrixMirror = Matrix()
        matrixMirror.preScale(-1.0f, 1.0f)
        return Bitmap.createBitmap(
            validBitmap, 0, 0, validBitmap.width, validBitmap.height, matrixMirror, false
        )
    }

    fun rotateBitmap(bitmap: Bitmap?, angle: Float): Bitmap {
        val validBitmap = imageValidator.validate(bitmap)
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(validBitmap, 0, 0, validBitmap.width, validBitmap.height, matrix, true)
    }

    @Throws(Exception::class)
    private fun getBitmapFromUri(uri: Uri): Bitmap? {

        val initialOptions = BitmapFactory.Options()
        initialOptions.inJustDecodeBounds = true

        var inputStream = resolver.openInputStream(uri)
        BitmapFactory.decodeStream(inputStream, null, initialOptions)
        val sampleSize = calculateSampleSize(initialOptions, IMAGE_MAX_SIZE)
        inputStream?.close()

        val options = BitmapFactory.Options()
        options.inSampleSize = sampleSize

        inputStream = resolver.openInputStream(uri)

        try {
            return BitmapFactory.decodeStream(inputStream, null, options)
        } catch (e: Exception) {
            throw BitmapConvertException(resourceProvider.getString(R.string.image_convertation_error))
        } finally {
            inputStream?.close()
        }
    }

    private fun calculateSampleSize(options: BitmapFactory.Options, imageMaxSize: Int): Int {
        var sampleSize = 1
        if (options.outHeight > imageMaxSize || options.outWidth > imageMaxSize) {
            sampleSize = Math.pow(
                2.0, Math.round(
                    Math.log(imageMaxSize / Math.max(options.outHeight, options.outWidth).toDouble()) / Math.log(0.5)
                ).toInt().toDouble()
            ).toInt()
        }
        return sampleSize
    }

    companion object {
        private const val IMAGE_MAX_SIZE = 600
    }
}