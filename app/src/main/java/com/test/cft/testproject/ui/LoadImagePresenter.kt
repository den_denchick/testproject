package com.test.cft.testproject.ui

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import com.arellomobile.mvp.InjectViewState
import com.test.cft.testproject.R
import com.test.cft.testproject.exception.LoadImageException
import com.test.cft.testproject.model.BitmapConverter
import com.test.cft.testproject.model.DrawableConverter
import com.test.cft.testproject.model.ImageListItemFactory
import com.test.cft.testproject.model.ImageLoadModel
import com.test.cft.testproject.repository.ImageLoadRepository
import com.test.cft.testproject.ui.adapter.ImageListItem
import com.test.cft.testproject.utils.ResourceProvider
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.functions.Functions
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import kotlin.random.Random

/**
 * @author Denis Kopylov
 */
@InjectViewState
class LoadImagePresenter @Inject constructor(
    private val bitmapConverter: BitmapConverter,
    private val drawableConverter: DrawableConverter,
    private val resourceProvider: ResourceProvider,
    private val imageListItemFactory: ImageListItemFactory,
    private val imageLoadModel: ImageLoadModel,
    private val imageLoadRepository: ImageLoadRepository
) : BasePresenter<LoadImageView>() {

    private var wasLoaded = false

    fun onConvertUriToBitmap(uri: Uri?) {
        Single.fromCallable { bitmapConverter.convertUriToBitmap(uri) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ img -> viewState.setBitmapToPreview(img) }, { error -> viewState.onError(error) })
            .disposeOnFinish()
    }

    fun onChooseBitmapFromCamera(bitmap: Bitmap?) {
        if (bitmap != null) {
            viewState.setBitmapToPreview(bitmap)
        } else {
            viewState.onError(LoadImageException(resourceProvider.getString(R.string.image_load_error)))
        }
    }

    fun onInverseColorBitmap(drawable: Drawable?) {
        Single.fromCallable { drawableConverter.convertDrawableToBitmap(drawable) }
            .map { bitmapConverter.inverseColorImage(it) }
            .map { imageListItemFactory.createImageListItem(it) }
            .doOnSuccess { emulatedProgress(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ item -> viewState.setImageListItem(item) }, { error -> viewState.onError(error) })
            .disposeOnFinish()

    }

    fun onRotateBitmap(drawable: Drawable?, angle: Float) {
        Single.fromCallable { drawableConverter.convertDrawableToBitmap(drawable) }
            .map { bitmapConverter.rotateBitmap(it, angle) }
            .map { imageListItemFactory.createImageListItem(it) }
            .doOnSuccess { emulatedProgress(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ item -> viewState.setImageListItem(item) }, { error -> viewState.onError(error) })
            .disposeOnFinish()
    }

    fun onMirrorBitmap(drawable: Drawable?) {
        Single.fromCallable { drawableConverter.convertDrawableToBitmap(drawable) }
            .map { bitmapConverter.mirrorBitmap(it) }
            .map { imageListItemFactory.createImageListItem(it) }
            .doOnSuccess { emulatedProgress(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ item -> viewState.setImageListItem(item) }, { error -> viewState.onError(error) })
            .disposeOnFinish()
    }

    fun loadImageFromUrl(url: String?) {
        viewState.setLoadFromUrl(true)
        val item = imageListItemFactory.createImageListItem(null)
        viewState.setImageListItem(item)
        Single.defer { imageLoadModel.loadImage(url) }
            .doOnSuccess { viewState.setBitmapToPreview(it) }
            .map { imageListItemFactory.createImageListItem(item.id, it, true) }
            .doOnSuccess { emulatedProgress(it) }
            .doAfterTerminate { viewState.setLoadFromUrl(false) }
            .subscribe(
                { itemList -> viewState.setUpdatedImageListItem(itemList) },
                { error -> viewState.onError(error) })
            .disposeOnFinish()
    }

    fun saveImages(items: List<ImageListItem>) {
        imageLoadRepository.saveImages(items)
    }

    fun loadData() {
        if (!wasLoaded) {
            Single.fromCallable { imageLoadRepository.getImages() }
                .flattenAsObservable { map -> map.entries }
                .map { imageListItemFactory.createImageListItem(it.key.toInt(), it.value, false) }
                .toList()
                .doOnSuccess { it.sortByDescending { imageListItem -> imageListItem.id } }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { items -> viewState.setItems(items) }
                .disposeOnFinish()
        }
        wasLoaded = true
    }

    private fun emulatedProgress(item: ImageListItem) {
        Observable.timer(Random.nextLong(3, 6), TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doAfterTerminate {
                viewState.setUpdatedImageListItem(
                    imageListItemFactory.onEmulatedProgress(item, false)
                )
            }
            .subscribe({ Functions.EMPTY_ACTION }, { error -> viewState.onError(error) })
            .disposeOnFinish()
    }
}
