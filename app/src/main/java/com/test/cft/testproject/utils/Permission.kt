package com.test.cft.testproject.utils

/**
 * @author Denis Kopylov
 */
interface Permission {
    val systemPermission: String

    val messageId: Int
}