package com.test.cft.testproject.utils

import android.app.Activity
import android.app.AlertDialog
import android.util.Log
import com.test.cft.testproject.BuildConfig
import com.test.cft.testproject.R
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ErrorHandler @Inject constructor(private val resourceProvider: ResourceProvider) {
    fun handle(activity: Activity, error: Throwable) {
        if (BuildConfig.DEBUG) {
            Log.e("TAG", error.message)
            error.printStackTrace()
        }

        AlertDialog.Builder(activity)
            .setMessage(ErrorUtils.obtainErrorMessage(resourceProvider, error))
            .setPositiveButton(R.string.common_ok) { dialog, _ -> dialog.dismiss() }
            .create()
            .show()
    }
}