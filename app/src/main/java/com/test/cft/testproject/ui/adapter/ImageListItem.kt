package com.test.cft.testproject.ui.adapter

import android.graphics.Bitmap

/**
 * @author Denis Kopylov
 */
data class ImageListItem(val id: Int, val bitmap: Bitmap?, val inProgress: Boolean)