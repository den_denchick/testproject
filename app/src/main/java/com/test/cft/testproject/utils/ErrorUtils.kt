package com.test.cft.testproject.utils

import com.test.cft.testproject.R
import com.test.cft.testproject.exception.BitmapConvertException
import com.test.cft.testproject.exception.LoadImageException

/**
 * @author Denis Kopylov
 */
object ErrorUtils {

    fun obtainErrorMessage(resourceProvider: ResourceProvider, error: Throwable): String {
        return when (error) {
            is LoadImageException, is BitmapConvertException -> error.message!!
            else -> resourceProvider.getString(R.string.error_message)
        }
    }
}