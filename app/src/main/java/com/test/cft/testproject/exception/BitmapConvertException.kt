package com.test.cft.testproject.exception

/**
 * @author Denis Kopylov
 */
class BitmapConvertException(message: String) : Exception(message)