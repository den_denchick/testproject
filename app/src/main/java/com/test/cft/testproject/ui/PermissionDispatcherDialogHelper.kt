package com.test.cft.testproject.ui

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.test.cft.testproject.R
import com.test.cft.testproject.utils.IntentUtils
import com.test.cft.testproject.utils.ProjectPermission

/**
 * @author Denis Kopylov
 */
class PermissionDispatcherDialogHelper(
    private val activity: Activity,
    private val permissionDialogObserver: PermissionDialogHelperObserver
) : DialogInterface.OnClickListener {

    private var finishOnCancel = false
    private var dialogAvailable = false

    override fun onClick(dialog: DialogInterface, which: Int) {
        if (which == DialogInterface.BUTTON_NEGATIVE) {
            dialog.cancel()
            if (finishOnCancel) {
                activity.finish()
            }
            return
        }

        if (dialogAvailable) {
            permissionDialogObserver.onNeedPermission()
        } else {
            activity.startActivity(IntentUtils.appSettings(activity))
        }
    }

    fun createDialog(permission: ProjectPermission, finishOnCancel: Boolean): AlertDialog {
        this.finishOnCancel = finishOnCancel
        dialogAvailable = ActivityCompat.shouldShowRequestPermissionRationale(activity, permission.systemPermission)
        val message = getFullDialogMessage(activity, permission)

        return createDialog(activity, message, this)
    }

    private fun createDialog(
        context: Context,
        message: String,
        clickListener: DialogInterface.OnClickListener
    ): AlertDialog {
        val builder = AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(R.string.common_permissions_allow, clickListener)
            .setNegativeButton(R.string.common_cancel, clickListener)

        return builder.create()
    }

    private fun getFullDialogMessage(context: Context, permission: ProjectPermission): String {
        var message = context.getString(permission.messageId)
        if (dialogAvailable) {
            message += context.getString(R.string.common_permissions_setting_suffix)
        }
        return message
    }
}
