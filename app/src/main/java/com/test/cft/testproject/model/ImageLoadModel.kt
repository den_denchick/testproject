package com.test.cft.testproject.model

import android.graphics.Bitmap
import com.test.cft.testproject.R
import com.test.cft.testproject.exception.LoadImageException
import com.test.cft.testproject.repository.ImageLoadRepository
import com.test.cft.testproject.utils.ResourceProvider
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ImageLoadModel @Inject constructor(
    private val imageLoadRepository: ImageLoadRepository,
    private val resourceProvider: ResourceProvider
) {

    fun loadImage(url: String?): Single<Bitmap> {
        if (url.isNullOrEmpty()) {
            throw LoadImageException(resourceProvider.getString(R.string.image_load_error))
        }
        return imageLoadRepository.loadImage(url)
    }
}