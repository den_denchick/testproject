package com.test.cft.testproject.ui.adapter

/**
 * @author Denis Kopylov
 */
interface ItemClickListener<T> {
    fun onClick(item: T)
}