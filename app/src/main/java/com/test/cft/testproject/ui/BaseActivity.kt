package com.test.cft.testproject.ui

import com.arellomobile.mvp.MvpActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.test.cft.testproject.App
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
abstract class BaseActivity<P: BasePresenter<*>> : MvpActivity() {

    abstract var presenter: P

    val component by lazy {
        (application as App).component!!.activityComponent()
    }
}
