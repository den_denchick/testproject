package com.test.cft.testproject.repository

import android.content.Context
import android.graphics.Bitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.test.cft.testproject.R
import com.test.cft.testproject.exception.LoadImageException
import com.test.cft.testproject.model.ImageValidator
import com.test.cft.testproject.ui.adapter.ImageListItem
import io.reactivex.Single
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ImageLoadRepository @Inject constructor(
    private val context: Context,
    private val imageValidator: ImageValidator,
    private val fileImageProvider: FileImageProvider
) {

    fun loadImage(url: String): Single<Bitmap> {
        return Single.create<Bitmap> { emitter ->
            Glide.with(context)
                .asBitmap()
                .load(url)
                .listener(object : RequestListener<Bitmap?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Bitmap?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        emitter.onError(LoadImageException(context.getString(R.string.image_load_error)))
                        return false
                    }

                    override fun onResourceReady(
                        resource: Bitmap?,
                        model: Any?,
                        target: Target<Bitmap?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        val validBitmap = imageValidator.validate(resource)
                        emitter.onSuccess(validBitmap)
                        return false
                    }
                })
                .preload()
        }
    }

    fun saveImages(items: List<ImageListItem>) {
        val map = HashMap<String, Bitmap>()
        items.forEach { item ->
            item.bitmap?.let { img ->
                map[item.id.toString()] = img
            }
        }

        fileImageProvider.putImages(map)
    }

    fun getImages(): Map<String, Bitmap> {
        return fileImageProvider.getImages()
    }
}