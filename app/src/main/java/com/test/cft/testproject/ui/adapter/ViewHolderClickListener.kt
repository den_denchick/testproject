package com.test.cft.testproject.ui.adapter

/**
 * @author Denis Kopylov
 */
interface ViewHolderClickListener {
    fun onClick(position: Int)
}