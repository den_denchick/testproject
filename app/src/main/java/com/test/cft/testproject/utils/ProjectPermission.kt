package com.test.cft.testproject.utils

import android.Manifest
import com.test.cft.testproject.R

/**
 * @author Denis Kopylov
 */
enum class ProjectPermission(override val systemPermission: String, override val messageId: Int) : Permission {

    CAMERA(
        Manifest.permission.CAMERA,
        R.string.perm_camera_dialog_message
    )
}
