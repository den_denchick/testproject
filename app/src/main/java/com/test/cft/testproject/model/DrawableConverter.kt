package com.test.cft.testproject.model

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import javax.inject.Inject


/**
 * @author Denis Kopylov
 */
class DrawableConverter @Inject constructor(private val imageValidator: ImageValidator) {

    fun convertDrawableToBitmap(drawable: Drawable?): Bitmap {
        val validDrawable = imageValidator.validate(drawable)
        if (validDrawable is BitmapDrawable) {
            return validDrawable.bitmap
        }

        var width = validDrawable.intrinsicWidth
        width = if (width > 0) width else 1
        var height = validDrawable.intrinsicHeight
        height = if (height > 0) height else 1

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        validDrawable.setBounds(0, 0, canvas.width, canvas.height)
        validDrawable.draw(canvas)

        return bitmap
    }
}
