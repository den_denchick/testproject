package com.test.cft.testproject.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings

/**
 * @author Denis Kopylov
 */
object IntentUtils {

    fun createPickImageFromGallery(vararg acceptedMimeTypes: String): Intent {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && acceptedMimeTypes.isNotEmpty()) {
            intent.putExtra(Intent.EXTRA_MIME_TYPES, acceptedMimeTypes)
        }
        return intent
    }

    fun imageCapture(): Intent {
        return Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    }

    fun appSettings(context: Context): Intent {
        return Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", context.packageName, null)
        )
    }
}