package com.test.cft.testproject.ui

import android.graphics.Bitmap
import com.arellomobile.mvp.MvpView
import com.test.cft.testproject.ui.adapter.ImageListItem

/**
 * @author Denis Kopylov
 */
interface LoadImageView : MvpView {

    fun setBitmapToPreview(bitmap: Bitmap?)

    fun setImageListItem(item: ImageListItem)

    fun setUpdatedImageListItem(item: ImageListItem)

    fun setLoadFromUrl(visible: Boolean)

    fun setItems(items: List<ImageListItem>)

    fun onError(error: Throwable)
}