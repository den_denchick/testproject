package com.test.cft.testproject.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.recyclerview.widget.RecyclerView

/**
 * @author Denis Kopylov
 */
class ImageListAdapter(
    private val itemClickListener: ItemClickListener<ImageListItem>
) : RecyclerView.Adapter<ImageListAdapter.ViewHolder>(), ViewHolderClickListener {

    var listItems: MutableList<ImageListItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                com.test.cft.testproject.R.layout.list_item_image,
                parent,
                false
            ),
            this
        )
    }

    override fun getItemCount() = listItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val backgroundColor =
            if (position % 2 == 0) com.test.cft.testproject.R.color.gray else com.test.cft.testproject.R.color.yellow
        holder.apply {
            val item = listItems[position]
            container.setBackgroundColor(ContextCompat.getColor(itemView.context, backgroundColor))
            progressBar.isGone = !item.inProgress
            imageItem.isGone = item.inProgress
            listItems[position].bitmap?.let { imageItem.setImageBitmap(it) }
        }
    }

    override fun onClick(position: Int) {
        itemClickListener.onClick(listItems[position])
    }

    fun additem(item: ImageListItem) {
        listItems.add(0, item)
        notifyDataSetChanged()
    }

    fun updateItem(item: ImageListItem) {
        val oldItem = listItems.find { it.id == item.id }
        if (oldItem != null) {
            val position = listItems.indexOf(oldItem)
            listItems.remove(oldItem)
            listItems.add(position, item)
            notifyItemChanged(position)
        }
    }

    fun removeItem(item: ImageListItem) {
        val position = listItems.indexOf(item)
        listItems.remove(item)
        notifyItemRemoved(position)
    }

    fun setItems(items: List<ImageListItem>) {
        this.listItems = ArrayList(items)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(itemView) {

        val progressBar: ProgressBar = itemView.findViewById(com.test.cft.testproject.R.id.progress)
        val imageItem: ImageView = itemView.findViewById(com.test.cft.testproject.R.id.image_item)
        val container: View = itemView.findViewById(com.test.cft.testproject.R.id.container)

        init {
            itemView.setOnClickListener { viewHolderClickListener.onClick(adapterPosition) }
        }
    }
}