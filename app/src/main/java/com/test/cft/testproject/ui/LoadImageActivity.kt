package com.test.cft.testproject.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.media.ExifInterface
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.test.cft.testproject.R
import com.test.cft.testproject.ui.adapter.ImageListAdapter
import com.test.cft.testproject.ui.adapter.ImageListItem
import com.test.cft.testproject.ui.adapter.ItemClickListener
import com.test.cft.testproject.utils.ErrorHandler
import com.test.cft.testproject.utils.IntentUtils
import com.test.cft.testproject.utils.ProjectPermission
import kotlinx.android.synthetic.main.inc_bottom_sheet.*
import kotlinx.android.synthetic.main.inc_content.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import javax.inject.Inject


@RuntimePermissions
class LoadImageActivity : BaseActivity<LoadImagePresenter>(), PermissionDialogHelperObserver, LoadImageView {

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var imageListAdapter: ImageListAdapter
    private var iconSelected = false
    private var loadFromUrl = false

    @Inject
    lateinit var errorHandler: ErrorHandler

    @Inject
    @InjectPresenter
    override lateinit var presenter: LoadImagePresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        component.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_load_image)
        colorizeStatusAndNavigationBar()
        initClickListeners()

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

        imageListAdapter = ImageListAdapter(object : ItemClickListener<ImageListItem> {
            override fun onClick(item: ImageListItem) {
                AlertDialog.Builder(this@LoadImageActivity)
                    .setMessage(R.string.message_dialog)
                    .setPositiveButton(R.string.convert_button) { _, _ -> setBitmapToPreview(item.bitmap) }
                    .setNegativeButton(R.string.remove_button) { _, _ -> imageListAdapter.removeItem(item) }
                    .create()
                    .show()
            }
        })

        imageList.layoutManager = LinearLayoutManager(this)
        imageList.adapter = imageListAdapter
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE_PICK_IMAGE_GALLERY) {
                presenter.onConvertUriToBitmap(data?.data)
            } else {
                presenter.onChooseBitmapFromCamera(data?.extras?.get(EXTRA_DATA) as Bitmap?)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.loadData()
    }

    override fun onStop() {
        presenter.saveImages(imageListAdapter.listItems)
        super.onStop()
    }

    override fun onBackPressed() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_COLLAPSED) {
            super.onBackPressed()
        } else {
            closeBottomSheetAndClearUrlField()
        }
    }

    override fun setBitmapToPreview(bitmap: Bitmap?) {
        imageChoose.setImageBitmap(bitmap)
        iconSelected = true
    }

    override fun setImageListItem(item: ImageListItem) {
        imageListAdapter.additem(item)
    }

    override fun setUpdatedImageListItem(item: ImageListItem) {
        imageListAdapter.updateItem(item)
    }

    override fun setItems(items: List<ImageListItem>) {
        imageListAdapter.setItems(items)
    }

    override fun onError(error: Throwable) {
        errorHandler.handle(this, error)
    }

    override fun onNeedPermission() {
        needsCameraPermissionWithPermissionCheck()
    }

    override fun setLoadFromUrl(visible: Boolean) {
        this.loadFromUrl = visible
    }

    @NeedsPermission(Manifest.permission.CAMERA)
    fun needsCameraPermission() {
        toogleBottomSheet()
    }

    @OnPermissionDenied(Manifest.permission.CAMERA)
    fun onCameraPermissionDenied() {
        createPermissionDialog()
    }

    @OnNeverAskAgain(Manifest.permission.CAMERA)
    fun onCameraPermissionNeverAskAgain() {
        createPermissionDialog()
    }

    private fun toogleBottomSheet() {
        bottomSheetBehavior.state = when (bottomSheetBehavior.state) {
            BottomSheetBehavior.STATE_EXPANDED -> BottomSheetBehavior.STATE_COLLAPSED
            else -> BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun closeBottomSheetAndClearUrlField() {
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        addressUrlContainer.visibility = View.GONE
        urlText.text = null
    }

    private fun toogleAddressUrlContainer() {
        when (addressUrlContainer.visibility) {
            View.GONE -> addressUrlContainer.visibility = View.VISIBLE
            View.VISIBLE -> addressUrlContainer.visibility = View.GONE
        }
    }

    private fun colorizeStatusAndNavigationBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val color = ContextCompat.getColor(this, R.color.yellow)
            window.statusBarColor = color
            window.navigationBarColor = color
        }
    }

    private fun createPermissionDialog() {
        PermissionDispatcherDialogHelper(this, this)
            .createDialog(ProjectPermission.CAMERA, true)
            .show()
    }

    private fun initClickListeners() {
        imageFromGalleryButton.setOnClickListener {
            closeBottomSheetAndClearUrlField()
            startActivityForResult(IntentUtils.createPickImageFromGallery(), REQUEST_CODE_PICK_IMAGE_GALLERY)
        }

        imageFromCameraButton.setOnClickListener {
            closeBottomSheetAndClearUrlField()
            startActivityForResult(IntentUtils.imageCapture(), REQUEST_CODE_PICK_IMAGE_CAMERA)
        }

        imageFromUrl.setOnClickListener {
            if (!loadFromUrl) {
                toogleAddressUrlContainer()
            }
        }

        urlButton.setOnClickListener {
            presenter.loadImageFromUrl(urlText.text.toString())
            closeBottomSheetAndClearUrlField()
        }

        invertColorButton.setOnClickListener {
            if (iconSelected) {
                presenter.onInverseColorBitmap(imageChoose.drawable)
            }
        }

        rotateButton.setOnClickListener {
            if (iconSelected) {
                presenter.onRotateBitmap(imageChoose.drawable, 90f)
            }
        }

        mirrorImageButton.setOnClickListener {
            if (iconSelected) {
                presenter.onMirrorBitmap(imageChoose.drawable)
            }
        }

        imageChoose.setOnClickListener { needsCameraPermissionWithPermissionCheck() }
    }

    companion object {
        private const val REQUEST_CODE_PICK_IMAGE_GALLERY = 0
        private const val REQUEST_CODE_PICK_IMAGE_CAMERA = 1
        private const val EXTRA_DATA = "data"
    }
}
