package com.test.cft.testproject.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * @author Denis Kopylov
 */
@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun activityComponent(): ActivityComponent

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}