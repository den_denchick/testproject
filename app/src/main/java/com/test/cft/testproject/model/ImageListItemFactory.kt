package com.test.cft.testproject.model

import android.graphics.Bitmap
import com.test.cft.testproject.ui.adapter.ImageListItem
import javax.inject.Inject

/**
 * @author Denis Kopylov
 */
class ImageListItemFactory @Inject constructor(private val idCreator: IdCreator) {

    fun createImageListItem(bitmap: Bitmap?): ImageListItem {
        return ImageListItem(idCreator.getId(), bitmap, true)
    }

    fun createImageListItem(id: Int, bitmap: Bitmap?, progress: Boolean): ImageListItem {
        return ImageListItem(id, bitmap, progress)
    }

    fun onEmulatedProgress(item: ImageListItem, progress: Boolean): ImageListItem {
        return ImageListItem(item.id, item.bitmap, progress)
    }
}