package com.test.cft.testproject.di

import javax.inject.Scope

/**
 * @author Denis Kopylov
 */
@Scope
@Retention(AnnotationRetention.SOURCE)
annotation class ActivityScope