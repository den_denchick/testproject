package com.test.cft.testproject.ui

/**
 * @author Denis Kopylov
 */
interface PermissionDialogHelperObserver {
    fun onNeedPermission()
}