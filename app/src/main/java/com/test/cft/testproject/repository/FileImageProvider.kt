package com.test.cft.testproject.repository

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject
import kotlin.collections.HashMap


/**
 * @author Denis Kopylov
 */
class FileImageProvider @Inject constructor(private val context: Context) {

    fun getImages(): Map<String, Bitmap> {
        val pref: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val ids = pref.getStringSet(APP_PREFERENCES_KEY_IDS, emptySet())!!
        val map = HashMap<String, Bitmap>()
        ids.forEach { idFile ->
            val bitmap = BitmapFactory.decodeFile("${context.cacheDir}/$idFile")
            bitmap?.let { img -> map[idFile] = img }
        }
        return map
    }

    fun putImages(map: Map<String, Bitmap>) {
        val pref: SharedPreferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
        val ids = pref.getStringSet(APP_PREFERENCES_KEY_IDS, emptySet())!!

        val set = map.keys.toSet()
        val editor = pref.edit()
        editor.putStringSet(APP_PREFERENCES_KEY_IDS, set)
        editor.apply()
        for (entry in map) {
            if (!ids.contains(entry.key)) {
                create(entry.key, entry.value)
            }
        }
    }

    private fun create(filename: String, bitmap: Bitmap) {
        val file = File(context.cacheDir, filename)
        file.createNewFile()
        val byteArrayOutputStream = ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val bitmapData = byteArrayOutputStream.toByteArray()
        val outputStream = FileOutputStream(file)
        outputStream.write(bitmapData)
        outputStream.flush()
        outputStream.close()
    }

    companion object {
        private const val APP_PREFERENCES = "pref"
        private const val APP_PREFERENCES_KEY_IDS = "ids"
    }
}