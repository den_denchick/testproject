package com.test.cft.testproject

import android.app.Application
import com.test.cft.testproject.di.AppComponent
import com.test.cft.testproject.di.DaggerAppComponent

/**
 * @author Denis Kopylov
 */
class App : Application() {

    var component: AppComponent? = null
        get() {
            if (field == null) {
                field = DaggerAppComponent.builder()
                    .application(this)
                    .build()
            }
            return field
        }
}
